<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CustomerController extends Controller
{
    public function getCustomer(Request $request){
        $id = $request->id;
        if($id != null){
            $getCustomer = DB::table('tabel_customer')
                    ->where('customerId',$id)
                    ->first();
        }else{
            $getCustomer = DB::table('tabel_customer')->get();
        }
        if($getCustomer){
            $response = [
                'msg' => 'Success',
                'msg_code' => 200,
                'customer' => $getCustomer,
            ];
        }else{
            $response = [
                'msg' => 'Gagal mengambil data',
                'msg_code' => 401,
                'data' => array(),
            ];
        }
        return response()->json($response);
    }
    public function customerPost(Request $request){
        $insert = DB::table('tabel_customer')
                ->insert([
                    'customerName' => $request->customerName,
                    'customerCity' => $request->customerCity,
                    'customerPhone' => $request->customerPhone,
                    'created_at' => now(),
                ]);
        if($insert){
            $response = [
                'msg' => 'Success',
                'msg_code' => 200,
                'status' => true,
            ];
        }else{
            $response = [
                'msg' => 'Gagal',
                'msg_code' => 401,
                'status' => false,
            ];
        }
        return response()->json($response);
    }
    public function customerUpdate(Request $request){
        $id = $request->id;
        $update = DB::table('tabel_customer')
                ->where('customerId',$id)
                ->update([
                    'customerName' => $request->customerName,
                    'customerCity' => $request->customerCity,
                    'customerPhone'  => $request->customerPhone,
                    'updated_at' => now(),
                ]);
        if($update){
            $response = [
                'msg' => 'Success',
                'msg_code' => 200,
                'status' => true,
            ];
        }else{
            $response = [
                'msg' => 'Gagal',
                'msg_code' => 401,
                'status' => false,
            ];
        }
        return response()->json($response);
    }
    public function customerDelete(Request $request){
        $id = $request->id;
        $delete = DB::table('tabel_customer')
                ->where('customerId',$id)
                ->delete();
        if($delete){
            $response = [
                'msg' => 'Success',
                'msg_code' => 200,
                'status' => true,
            ];
        }else{
            $response = [
                'msg' => 'Gagal',
                'msg_code' => 401,
                'status' => false,
            ];
        }
        return response()->json($response);
    }
}
